# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm install`

Após clonar esse projeto, você precisará rodar npm install em seu terminal para instalar as dependências que o projeto necessita para inicializar.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### Informações do projeto

### `css`

Utilizei para cada componente o cssModules porém criei um arquivo global contendo as variáveis de cor, font e também deixei o os breakpoints para me auxiliar ao fazer a responsividade do projeto. Esse arquivo se encontra dentro da pasta css.