import { useCallback, useEffect, useState } from 'react';

import { Header } from '../../components/header';
import { LocationPages } from '../../components/locationPages';
import { SideBar } from '../../components/sideBar';
import { BackToTop } from '../../components/back-top-top';
import { Footer } from '../../components/footer';

import style from './style.module.css';

export function Home() {
  const [ showComponentBackToTop, setShowComponentBackToTop ] = useState<boolean>(false);

  const checkScroolTop = useCallback(() => { 
    !showComponentBackToTop && window.scrollY > 300 ? 
      setShowComponentBackToTop(true) : setShowComponentBackToTop(false)
  }, [])

  useEffect(() => {
    window.addEventListener('scroll', checkScroolTop)
  }, [])

  return (
    <>
      <Header />

      <main id={style.institutional}>
        <LocationPages pageName="institucional"/>

        <h1>Institucional</h1>

        <div className={style['institutional-container']}>
          <SideBar /> 
        </div>
      </main>

      <BackToTop isShow={showComponentBackToTop} />

      <Footer />
    </>
  );
}