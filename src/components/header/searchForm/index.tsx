import { useCallback } from 'react';
import { 
  Formik, 
  Form, 
  Field,
} from 'formik';

import style from './style.module.css';

import bloomImageUrl from '../../../assets/bloom.svg';

interface ISearchFormProps {
  formClass: 'header-form-mobile' | 'header-form'
}

interface IFormikSearchValue {
  search: string;
}

export function SearchForm({ formClass }:ISearchFormProps) {
  const handleSubmitSearch = useCallback((value: IFormikSearchValue) => {
    console.log(value)
  }, [])

  return (
    <Formik onSubmit={handleSubmitSearch} initialValues={{ search: '' }}>
      <Form className={style[`${formClass}`]}>
        <div className={style['input-wrapper']}>
          <label
            htmlFor="search"
            className="sr-only"
          >
            Busca
          </label>

          <Field
            type="text"
            name="search"
            id="search"
            placeholder="Buscar..."
          />
        </div>

        <button
          type="submit"
        >
          <img
            src={bloomImageUrl}
            alt="clique aqui para buscar o que deseja"
          />
        </button>
      </Form>
    </Formik>
  );
}