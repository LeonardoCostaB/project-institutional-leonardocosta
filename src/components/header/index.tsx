import React, { useCallback, useState } from 'react';
import classNames from 'classnames';

import { SearchForm } from './searchForm';

import style from './style.module.css';

import logom3ImageUrl from '../../assets/logo-m3.svg';
import cartImageUrl from '../../assets/cart.svg';
import menuHamburguerImageUrl from '../../assets/menu-hamburguer.svg';

export function Header() {
  const [ isOpenMenuMobile, setIsOpenMenuMobile ] = useState<boolean>(false);

  return (
    <header className={style['page-header']}>
      <div className={style['header-apresentation']}>
        <div className={style['header-container']}>
          <button 
            type="button"
            className={style['menu-hamburguer']}
            onClick={() => setIsOpenMenuMobile(true)}
          >
            <img 
              src={menuHamburguerImageUrl} 
              alt="clique aqui para abrir o menu" 
            />
          </button>

          <a 
            href="/"
            className={style['logo-m3']}
          >
            <img 
              src={logom3ImageUrl} 
              alt="logo escrito m3academy" 
            />
            <strong className={style['academy']}>academy</strong>
          </a>

          <SearchForm formClass="header-form" />
          
          <div className={style['header-login']}>
            <a 
              href="/#"
              className={style['user-login']}  
            >
              entrar
            </a>

            <button
              type="button"
              className={style.cart}
            >
              <img 
                src={cartImageUrl} 
                alt="clique aqui para acessar o seu carrinho de compra"
              />
            </button>

          </div>
        </div>
      </div>

      <nav
        className={
          classNames(style['header-navigation'], {
            [style.show]: isOpenMenuMobile
          })
        }
        onClick={() => setIsOpenMenuMobile(false)}
      >
        <div className={style['header-container']}>
          <ul 
            className={
              classNames(style['navigation-container'], {
                [style.open]: isOpenMenuMobile
              })
            }
          >
            <li className={style['navigation-links']}>
              <a href="/#">
                cursos
              </a>
            </li>
            
            <li className={style['navigation-links']}>
              <a href="/#">
                saiba mais
              </a>
            </li>
          </ul>
        </div>
      </nav>

      <div className={style['header-search-mobile']}>
        <div className={style['header-container']}>
          <SearchForm formClass="header-form-mobile" />
        </div>
      </div>
    </header>
  );
}