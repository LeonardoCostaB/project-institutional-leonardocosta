import React, { useState } from 'react';
import classNames from 'classnames';

import { ContentSideBar } from './contentSideBar';

import style from './style.module.css';

export function SideBar() {
  const [ valueClickSideBar, setValueClickSideBar ] = useState('about');

  return(
    <>
      <aside className={style.sideBar}>
        <ul>
          <li>
            <button
              className={
                classNames(style['button-sideBar'], {
                  [style.active]: valueClickSideBar === 'about'
                })
              }
              onClick={() => setValueClickSideBar('about')}
            >
              Sobre
            </button>
          </li>

          <li>
            <button className={style['button-sideBar']}>
              Forma de Pagamento
            </button>
          </li>

          <li>
            <button className={style['button-sideBar']}>
              Entrega
            </button>
          </li>

          <li>
            <button className={style['button-sideBar']}>
              Troca e Devolução
            </button>
          </li>

          <li>
            <button className={style['button-sideBar']}>
              Segurança e Privacidade
            </button>
          </li>

          <li>
            <button 
              className={
                classNames(style['button-sideBar'], {
                  [style.active]: valueClickSideBar === 'contact'
                })
              }
              onClick={() => setValueClickSideBar('contact')}
            >
              Contato
            </button>
          </li>
        </ul>
      </aside>

      <ContentSideBar 
        isOpenSectionCurrentySideBar={valueClickSideBar}
      />
    </>
  );
}