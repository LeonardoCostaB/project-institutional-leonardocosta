import { useCallback } from 'react';
import { Form, Field, Formik, ErrorMessage, FormikHelpers } from 'formik';

import schemaContact  from '../../../schema/formContact';

import style from './style.module.css';

interface IContentSideBarProps {
  isOpenSectionCurrentySideBar: string
}

interface IFormikContactValues {
  useName: string,
  useEmail: string,
  useDocument: string,
  useBrithDate: string,
  useMobile: string,
  useInstagram: string,
  acceptTerms: boolean
}

const initialValuesFormik = {
  useName: '',
  useEmail: '',
  useDocument: '',
  useBrithDate: '',
  useMobile: '',
  useInstagram: '',
  acceptTerms: false
}

export function ContentSideBar({ isOpenSectionCurrentySideBar }: IContentSideBarProps) {
  const handleSubmitContact = useCallback((
    value: IFormikContactValues, 
    { resetForm }: FormikHelpers<IFormikContactValues>
  ) => {
    console.log(value)

    resetForm()
  }, [])  

  return(
    <>
      { isOpenSectionCurrentySideBar === 'about' && (
        <section id={style['about']}>
          <h2>Sobre</h2>

          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. <br />
            <br />
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. <br />
            <br />
            <span className={style['about-desktop']}>
              Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
            </span>
          </p>
        </section>
      )}

      { isOpenSectionCurrentySideBar === 'contact' && (
        <section id={style.contact}>
          <Formik 
            onSubmit={handleSubmitContact}
            initialValues={initialValuesFormik}
            validationSchema={schemaContact}
          >
            { ({ errors, touched, isValid, dirty }) => (
              <Form className={style['form-contact']}>
                <fieldset className={style['form-group']}>
                  <legend>
                    preencha o formulário
                  </legend>

                  <div className={style['input-wrapper']}>
                    <label htmlFor="useName">
                      Nome
                    </label>

                    <Field 
                      type="text"
                      name="useName"
                      id="useName"
                      placeholder="Seu nome completo"
                      className={
                        errors.useName && touched.useName && style['error-input']
                      }
                    />

                    <ErrorMessage 
                      component="span"
                      name="useName"
                      className={ style['error-message'] }
                    />
                  </div>

                  <div className={style['input-wrapper']}>
                    <label htmlFor="useEmail">E-mail</label>

                    <Field 
                      type="email"
                      name="useEmail"
                      id="useEmail"
                      placeholder="Seu e-mail"
                      className={
                        errors.useEmail && touched.useEmail && style['error-input']
                      }
                    />

                    <ErrorMessage 
                      component="span"
                      name="useEmail"
                      className={ style['error-message'] }
                    />
                  </div>

                  <div className={style['input-wrapper']}>
                    <label htmlFor="useDocument">CPF</label>

                    <Field 
                      type="text"
                      name="useDocument"
                      id="useDocument"
                      placeholder="000 000 000 00"
                      className={
                        errors.useDocument && touched.useDocument && style['error-input']
                      }
                    />

                    <ErrorMessage 
                      component="span"
                      name="useDocument"
                      className={ style['error-message'] }
                    />
                  </div>

                  <div className={style['input-wrapper']}>
                    <label htmlFor="useBrithDate">Data de Nascimento</label>

                    <Field 
                      type="text"
                      name="useBrithDate"
                      id="useBrithDate"
                      placeholder="00 . 00 . 0000"
                      className={
                        errors.useBrithDate && touched.useBrithDate && style['error-input']
                      }
                    />

                    <ErrorMessage 
                      component="span"
                      name="useBrithDate"
                      className={ style['error-message'] }
                    />
                  </div>

                  <div className={style['input-wrapper']}>
                    <label htmlFor="useMobile">Telefone:</label>

                    <Field
                      type="text"
                      name="useMobile"
                      id="useMobile"
                      placeholder="(+00) 00000 0000"
                      maxLength={15}
                      className={
                        errors.useMobile && touched.useMobile && style['error-input']
                      }
                    />

                    <ErrorMessage 
                      component="span"
                      name="useMobile"
                      className={ style['error-message'] }
                    />
                  </div>

                  <div className={style['input-wrapper']}>
                    <label htmlFor="useInstagram">Instagram</label>

                    <Field 
                      type="text"
                      name="useInstagram"
                      id="useInstagram"
                      placeholder="@seuuser"
                      className={
                        errors.useMobile && touched.useMobile && style['error-input']
                      }
                    />

                    <ErrorMessage 
                      component="span"
                      name="useInstagram"
                      className={ style['error-message'] }
                    />
                  </div>
                </fieldset>

                <fieldset className={style['input-checkbox']}>
                  <div className={style['checkbox-wrapper']}>
                    <label htmlFor="acceptTerms">
                      <span>*</span>Declaro que li e aceito
                    </label>

                    <Field
                      type="checkBox"
                      name="acceptTerms"
                      id="acceptTerms"
                      className={
                        errors.useMobile && touched.useMobile && style['error-input']
                      }
                    />
                  </div>

                  <ErrorMessage 
                    name="acceptTerms"
                    component="span"
                    className={ style['error-message']}
                  />
                </fieldset>

                <button
                  type="submit"
                  className={style['contact-submit']}
                  disabled={ !(dirty && isValid) }
                >
                  cadastre-se
                </button>
              </Form>
            )}

          </Formik> 
        </section>
      )}
    </>
  );
}