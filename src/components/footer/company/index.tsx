import React, { useState } from 'react';

import classNames from 'classnames';

import style from './style.module.css';

import facebookIconImgUrl from '../../../assets/facebook.svg';
import instagramIconImgUrl from '../../../assets/instagram.svg';
import twitterIconImgUrl from '../../../assets/twitter.svg';
import youtubeIconImgUrl from '../../../assets/youtube.svg';
import linkedinIconImgUrl from '../../../assets/linkedin.svg';
import buttonOpenIconImgUrl from '../../../assets/button-open.svg';

const networkings = {
  facebook: {
    img: {
      src: facebookIconImgUrl,
      alt: 'clique aqui para acessar nosso facebook',
      title: 'rede social facebook'
    },

    link: 'https://www.facebook.com/digitalm3/'
  },

  instagram: {
    img: {
      src: instagramIconImgUrl,
      alt: 'clique aqui para acessar nosso instagram',
      title: 'rede social instagram'
    },

    link: 'https://www.instagram.com/m3.ecommerce/'
  },

  twitter: {
    img: {
      src: twitterIconImgUrl,
      alt: 'clique aqui para acessar nosso twitter',
      title: 'rede social twitter'
    },

    link: 'https://twitter.com'
  },

  youtube: {
    img: {
      src: youtubeIconImgUrl,
      alt: 'clique aqui para acessar nosso youtube',
      title: 'rede social youtube'
    },

    link: 'https://www.youtube.com/channel/UCSMoUkouJCf_LUYfFiIK2yw'
  },

  linkedin: {
    img: {
      src: linkedinIconImgUrl,
      alt: 'clique aqui para acessar nosso linkedin',
      title: 'rede social linkedin'
    },

    link: 'https://www.linkedin.com/company/m3ecommerce/'
  }
}

export function FooterCompany() {
  const [isOpenListInstitutional,  SetIsOpenListInstitutional ] = useState<boolean>(false);
  const [isOpenListDoubts,  SetIsOpenListDoubts ] = useState<boolean>(false);
  const [isOpenListContactUs,  SetIsOpenListContactUs ] = useState<boolean>(false);

  return (
    <section className={style.company}>
      <div className={style['company-container']}>
        <div className={style['company-institutional']}>
          <div className={style['container-list']}>
            <div className={style['title-list']}>
              <strong className={style.title}>
                institucional
              </strong>

              <button
                type="button"
                className={style['open-list']}
                onClick={() => SetIsOpenListInstitutional(!isOpenListInstitutional)}
              >
                <img 
                  src={buttonOpenIconImgUrl} 
                  alt="clique aqui para acessar mais informações" 
                />
              </button>
            </div>

            <ul 
              aria-label="institucional"
              className={
                classNames(style['content-institutional'], {
                  [style.show]: isOpenListInstitutional
                })
              }
            >
              <li className={style['list-institutional']}>
                <a href="/#">
                  Quem Somos
                </a>
              </li>
              
              <li className={style['list-institutional']}>
                <a href="/#">
                  Política de Privacidade
                </a>
              </li>

              <li className={style['list-institutional']}>
                <a href="/#">
                  Segurança
                </a>
              </li>

              <li className={style['list-institutional']}>
                <a href="/#">
                  Seja um Revendedor
                </a>
              </li>
            </ul>
          </div>
          
          <div className={style['container-list']}>
            <div className={style['title-list']}>
              <strong className={style.title}>
                dúvidas
              </strong>

              <button
                type="button"
                className={style['open-list']}
                onClick={() => SetIsOpenListDoubts(!isOpenListDoubts)}
              >
                <img 
                  src={buttonOpenIconImgUrl} 
                  alt="clique aqui para acessar mais informações" 
                />
              </button>
            </div>

            <ul 
              aria-label="dúvidas"
              className={
                classNames(style['content-institutional'], {
                  [style.show]: isOpenListDoubts
                })
              }
            >
              <li className={style['list-institutional']}>
                <a href="/#">
                  Entrega
                </a>
              </li>
              
              <li className={style['list-institutional']}>
                <a href="/#">
                  Pagamento
                </a>
              </li>
              
              <li className={style['list-institutional']}>
                <a href="/#">
                  Trocas e Devoluções
                </a>
              </li>
              
              <li className={style['list-institutional']}>
                <a href="/#">
                  Dúvidas Frequentes
                </a>
              </li>
            </ul>
          </div>
          
          <div className={style['container-list']}>
            <div className={style['title-list']}>
              <strong className={style.title}>
                fale conosco
              </strong>

              <button
                type="button"
                className={style['open-list']}
                onClick={() => SetIsOpenListContactUs(!isOpenListContactUs)}
              >
                <img 
                  src={buttonOpenIconImgUrl} 
                  alt="clique aqui para acessar mais informações" 
                />
              </button>
            </div>

            <ul
              aria-label="fale conosco"
              className={
                classNames(style['content-institutional'], {
                  [style.show]: isOpenListContactUs
                })
              }
            >
              <li className={style['list-attendance']}>
                Atendimento ao Consumidor
              </li>

              <li className={style['list-attendance']}>
                <a href="/#">
                  (11) 4159 9504
                </a>
              </li>
              
              <li className={style['list-attendance']}>
                Atendimento Online
              </li>

              <li className={style['list-attendance']}>
                <a href="/#">
                  (11) 99433-8825
                </a>
              </li>
            </ul>
          </div>
        </div>
        
        <div className={style['company-networking']}>
          <ul className={style['content-networking']}>
            { Object.entries(networkings).map(([ key, value ]) => {
              return <li key={key}>
                  <a 
                    href={value.link} 
                    target="_blank"
                    rel="noreferrer"
                  >
                    <img 
                      src={value.img.src} 
                      alt={value.img.alt}
                      title={value.img.title} 
                    />
                  </a>
              </li>
            })}
          </ul>

          <a 
            href="/#"
            className={style['link-company']}
          >
            www.loremipsum.com
          </a>
        </div>
      </div>
    </section>
  );
}