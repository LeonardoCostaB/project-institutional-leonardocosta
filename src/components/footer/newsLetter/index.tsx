import React, { useCallback } from 'react';

import { Formik, Form, Field, ErrorMessage, FormikHelpers  } from 'formik';
import schemaNewsLetter from '../../../schema/formNewsLetter';

import style from './style.module.css';

interface IFormikNewsLetterValue {
  email: string
}

export function NewsLetter() {
  const handleSubmitNewsLetter = useCallback((
    value: IFormikNewsLetterValue,
    { resetForm }: FormikHelpers<IFormikNewsLetterValue> 
  ) => {
    console.log(value)

    resetForm()
  }, [])

  return (
    <section className={style.newsLetter}>
      <div className={style['newsLetter-container']}>
        <h2>assine nossa newsletter</h2>

        <Formik 
          onSubmit={handleSubmitNewsLetter} 
          initialValues={{ email: '' }}
          validationSchema={schemaNewsLetter}
          >
            {({ errors, touched, isValid, dirty }) => (
              <Form className={style['form-newsLetter']}>
                <div className={style['input-wrapper']}>
                  <label
                    htmlFor="email"
                    className="sr-only"
                  >
                    Email..
                  </label>

                  <Field
                    type="email"
                    name="email"
                    id="email"
                    placeholder="Email.."
                    className={
                      errors.email && touched.email && style['error-input']
                    }
                  />

                  <ErrorMessage 
                    name="email"
                    component="span"
                    className={style['error-message']}
                  />
                </div>

                <button
                  type="submit"
                  disabled={!(isValid && dirty)}
                >
                  enviar
                </button>
              </Form>
            )}
        </Formik>
      </div>
    </section>
  );
}