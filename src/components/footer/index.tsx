import React from 'react';

import { FooterCompany } from './company';
import { NewsLetter } from './newsLetter';

import style from './style.module.css';

import masterImgUrl from '../../assets/master.svg';
import visaImgUrl from '../../assets/visa.svg';
import dinersImgUrl from '../../assets/diners.svg';
import eloImgUrl from '../../assets/elo.svg';
import hiperImgUrl from '../../assets/hiper.svg';
import pagseguroImgUrl from '../../assets/pagseguro.svg';
import boletoImgUrl from '../../assets/boleto.svg';
import vtexPciImgUrl from '../../assets/vtex-pci-200.svg';
import logoVtexImgUrl from '../../assets/vtex-logo.svg';
import logoM3ImgUrl from '../../assets/logo-m3-footer.svg';

const paymentMethod = {
  master: {
    img: {
      src: masterImgUrl,
      alt: 'aceitamos a bandeira masterCard',
      title: 'bandeira masterCard'
    }
  },

  visa: {
    img: {
      src: visaImgUrl,
      alt: 'aceitamos a bandeira visa',
      title: 'bandeira visa'
    }
  },

  american: {
    img: {
      src: dinersImgUrl,
      alt: 'aceitamos a bandeira american express',
      title: 'bandeira american express'
    }
  },

  elo: {
    img: {
      src: eloImgUrl,
      alt: 'aceitamos a bandeira elo',
      title: 'bandeira elo'
    }
  },

  hiper: {
    img: {
      src: hiperImgUrl,
      alt: 'aceitamos a bandeira hiperCard',
      title: 'bandeira hiperCard'
    }
  },

  payPal: {
    img: {
      src: pagseguroImgUrl,
      alt: 'aceitamos pagamento via payPal',
      title: 'payPal'
    }
  },

  boleto: {
    img: {
      src: boletoImgUrl,
      alt: 'aceitamos pagamento via boleto',
      title: 'boleto'
    }
  },
}

export function Footer() {
  return (
    <footer>
      <NewsLetter />

      <FooterCompany />
      
      <div className={style.informations}>
        <p className={style.description}>
          Lorem ipsum dolor sit amet, consectetur adipiscing <br />
          elit<span>, sed do eiusmod tempor</span>
        </p>

        <div className={style['informations-payment']}>
          <ul className={style['payment-method']}>
            { Object.entries(paymentMethod).map(([ key, value ]) => {
              return <li key={key}>
                <img
                  src={value.img.src}
                  alt={value.img.alt}
                  title={value.img.title}
                />
              </li>
            })}
          </ul>

          <div className={style['line-divider']}></div>

          <span className={style['vtex-pci']}>
            <img
              src={vtexPciImgUrl}
              alt="pci é uma certificação que a vtex possui pela proteção com seus dados"
              title="vtex-pci-200"
            />
          </span>
        </div>

        <div className={style['developer-company']}>
          <a
            href="https://vtex.com/br-pt/"
            target="_blank"
            rel="noreferrer"
            className={style['powered-by']}
          >
            Powered by
            <img
              src={logoVtexImgUrl}
              alt="clique aqui para conhecer a plataforman da vtex"
              title="plataforma vtex"
            />
          </a>

          <a
            href="https://m3ecommerce.com/"
            target="_blank"
            rel="noreferrer"
            className={style['developed-by']}
          >
            Developed by
            <img
              src={logoM3ImgUrl}
              alt="clique aqui para conhecer o site da m3-ecommerce"
              title="m3-ecommerce"
              className={style['logo-footer']}
            />
          </a>
        </div>
      </div>
    </footer>
  )
}