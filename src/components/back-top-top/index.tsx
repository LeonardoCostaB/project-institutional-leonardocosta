import classNames from 'classnames';

import style from './style.module.css';

import whatsappIconUrl from '../../assets/whatsapp.svg';
import arrowTopIconUrl from '../../assets/arrow-top.svg';
import { useCallback } from 'react';

interface IBackToTopPros {
  isShow: boolean;
}

export function BackToTop({ isShow }: IBackToTopPros) {
  const scrollToTop = useCallback(() => {
    window.scrollTo({
      top: 0, 
      behavior: 'smooth'
    });
  }, []);
  
  return (
    <div className={
      classNames(style['container-buttons'], {
        [style.show]: isShow,
        [style['container-buttons']]: !isShow
      })
    }>
      <a 
        href="https://wa.me/" 
        target="_blank" 
        rel="noreferrer"
        className={style['button-whatsapp']}
      >
        <img 
          src={whatsappIconUrl}
          alt="Clique aqui para entrar em contato conosco via whatsapp" 
        />
      </a>

      <button
        type="button"
        className={style['back-to-top']}
        onClick={scrollToTop}
      >
        <img 
          src={arrowTopIconUrl} 
          alt="clique aqui para voltar ao topo da página" 
        />
      </button>
    </div>
  );
}