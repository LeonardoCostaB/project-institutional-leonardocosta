import style from './style.module.css';

import houseIconImgUrl from '../../assets/house.svg';
import arrowLeftIconImgUrl from '../../assets/arrow-left.svg';

interface ILocationPagesProps {
  pageName: string;
}

export function LocationPages({ pageName }: ILocationPagesProps) {
  return(
    <div className={style['location-page']}>
      <img src={houseIconImgUrl} alt="ícone de uma casa" />

      <img src={arrowLeftIconImgUrl} alt="seta apontando para a direita" />

      <span>{ pageName }</span>
    </div>
  );
}