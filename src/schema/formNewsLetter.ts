import * as yup from 'yup';

export default yup.object().shape({
  email: yup
    .string()
    .required('Por favor, digite o seu email')
    .email('Por favor, digite um email válido')
})