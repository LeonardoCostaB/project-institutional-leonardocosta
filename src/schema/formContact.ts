import * as yup from 'yup';
import { cpf } from 'cpf-cnpj-validator';

export default yup.object().shape({
  useName: yup
    .string()
    .required('Por favor, digite o seu nome')
    .min(3, 'Digite um nome que tenha mais de dois caracteres'),

  useEmail: yup
    .string()
    .required('Por favor, digite seu email')
    .email('Por favor, digite um email válido'),

  useDocument: yup
    .string()
    .required('Por favor, digite o seu CPF')
    .test(
      'is-valid-document', 
      'cpf inválido', 
      value => cpf.isValid(value as string)
    ),

  useBrithDate: yup
    .string()
    .required('Por favor, digite sua data de nascimento')
    .min(10, 'formato de data inválido')
    .max(10),

  useMobile: yup
    .string()
    .required('Por favor, digite seu número de telefone')
    .min(15, 'formato de número inválido')
    .max(15),

  useInstagram: yup
    .string()
    .required('Por favor, digite o nome do seu usuário'),

  acceptTerms: yup
    .boolean()
    .oneOf([true], 'aceite para continuar')
})